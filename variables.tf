variable "cloud_id" {
  type = string
}

variable "folder_id" {
  type = string
}

variable "user" {
  type    = string
  default = "vvot46"
}

variable "tgkey" {
  type = string
}